'''
Created on Sep 6, 2011

@author: michael
'''

import sys, xbmcaddon
from resources.lib.View import VideoView, SelectionView
from resources.lib.VDRPersistence import VDRRecording

Addon = xbmcaddon.Addon('script.vdrcutter')

__scriptname__ = Addon.getAddonInfo('name')
__id__ = Addon.getAddonInfo('id')
__author__ = Addon.getAddonInfo('author')
__version__ = Addon.getAddonInfo('version')
__path__ = Addon.getAddonInfo('path')

print '[SCRIPT][%s] version %s initialized!' % (__scriptname__, __version__)

if (__name__ == "__main__"):
    view = SelectionView('script-%s-selection.xml' % __scriptname__, __path__, 'default')
    view.doModal()
    print '[SCRIPT][%s] version %s exited!' % (__scriptname__, __version__)
    result = view.Recording
    del view
    
    if type(result) == VDRRecording:
        videoView = VideoView('script-%s-main.xml' % __scriptname__, __path__, 'default')
        videoView.Recording = result
        videoView.doModal()
        del videoView
    
    #videoView = DynamicVideoView()
    #videoView.doModal()
    #del videoView
    
sys.modules.clear()
