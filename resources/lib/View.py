'''
Created on Sep 15, 2011

@author: michael
'''

import sys, xbmcgui, xbmc
from VDRPersistence import VDRRecording, RestfulAPI, VDRMark

class SelectionView(xbmcgui.WindowXMLDialog):
    def __init__(self, *args, **kwargs):
        xbmcgui.WindowXMLDialog.__init__(self, *args, **kwargs)
        Addon = sys.modules['__main__'].Addon

        self.Settings = Addon.getSetting
        self.Strings = Addon.getLocalizedString

    def onInit(self):
        self.action_exitkeys_id = [10, 13]

        self.bEdit = self.getControl(3000)
        self.bCut = self.getControl(3001)
        self.lTitle = self.getControl(3005)
        self.lStatus = self.getControl(3021)
        self.lDebug = self.getControl(3020)

        self.bEdit.setLabel(self.Strings(3108))
        self.bCut.setLabel(self.Strings(3109))
        self.lTitle.setLabel(self.Strings(3110))

        self.Recording = None

        self.lRecordings = self.getControl(3002)

        self.RestfulAPI = RestfulAPI("192.168.1.6", 8002)

        self.Recordings = self.RestfulAPI.getRecordings()

        self.Recordings = sorted(self.Recordings, cmp=VDRRecording.compare)

        self.updateCutterStatus()

        for rec in self.Recordings:
            #print("rec.name : %s" % rec.Name)
            self.lRecordings.addItem(rec.Name)

    def onAction(self, action):
        if action in self.action_exitkeys_id:
            self.Mode = False
            self.close()

    def onClick(self, controlId):
        if controlId == 3002:
            Control.SetFocus(3000, 0)
        if controlId == 3000:
            pos = self.lRecordings.getSelectedPosition()
            self.Recording = self.Recordings[pos]
            self.close()
        if controlId == 3001:
            self.cutRecording()
            self.lStatus.setLabel("VDR Cutter Status: Active")

    def __getRecording(self):
        return self.__recording

    def __setRecording(self, recording):
        self.__recording = recording

    def onFocus(self, controlId):
        pass

    def cutRecording(self):
        pos = self.lRecordings.getSelectedPosition()
        self.RestfulAPI.refreshRecording(self.Recordings[pos]) #update ID if it has changed (recording ID's are dynamic)
        rec = self.Recordings[pos].RecordingId
        self.RestfulAPI.cutRecording(rec)

    def updateCutterStatus(self):
        status = "VDR Cutter Status: "

        if self.RestfulAPI.isCutterActive():
            status += "Active"
        else:
            status += "Inactive"

        self.lStatus.setLabel(status)

    Recording = property(__getRecording, __setRecording)

class VideoView(xbmcgui.WindowXML):
    def __init__(self, *args, **kwargs):
        xbmcgui.WindowXML.__init__(self, *args, **kwargs)
        Addon = sys.modules['__main__'].Addon

        self.Settings = Addon.getSetting
        self.Strings = Addon.getLocalizedString
        self.Dialog = xbmcgui.Dialog()
        self.Player = xbmc.Player()

        self.paused = False

    def onInit(self):
        self.action_exitkeys_id = [10, 13]

        self.bPrev = self.getControl(3000)
        self.bRewind = self.getControl(3001)
        self.bPlayPause = self.getControl(3002)
        self.bForward = self.getControl(3003)
        self.bNext = self.getControl(3004)
        self.bAddMark = self.getControl(3005)
        self.lMarks = self.getControl(3010)
        self.lTitle = self.getControl(3011)
        self.lDebug = self.getControl(3020)

        self.lTitle.setLabel(self.__recording.Name)

        self.RestfulAPI = RestfulAPI("192.168.1.6", 8002)

        self.Marks = self.RestfulAPI.getMarks(self.__recording.RecordingId)
        self.lMarks.reset()
        for m in self.Marks:
            self.lMarks.addItem(m.toString())

        self.playRecording()

        self.CutLineControls = []

        self.updateCutLine()

    def onAction(self, action):
        if action in self.action_exitkeys_id:
            if self.Player.isPlaying() or self.paused:
                self.Player.stop()
            self.saveMarks()
            self.close()
    def onFocus(self, controlId):
        pass

    def onClick(self, controlId):
        if controlId == 3000:
            self.Player.seekTime((self.Player.getTime()-(3*60)))
            self.pauseRecording()
        if controlId == 3001:
            self.Player.seekTime((self.Player.getTime()-1))
            self.pauseRecording()
        if controlId == 3002:
            self.playRecording()
        if controlId == 3003:
            self.Player.seekTime((self.Player.getTime()+1))
            self.pauseRecording()
        if controlId == 3004:
            self.Player.seekTime((self.Player.getTime()+(3*60)))
            self.pauseRecording()
        if controlId == 3005:
            self.addMark()
        if controlId == 3007:
            self.jumpToPosition()
        if controlId == 3006:
            self.removeSelectedMark()
    
    def pauseRecording(self):
        print "----------------"
        print "isPlaying %s" % self.Player.isPlaying()
        print "pause %s" % self.paused
        if self.Player.isPlaying() and not self.paused:
                print "dans le if"
                self.Player.pause()
                self.paused = True
                print "isPlaying %s" % self.Player.isPlaying()
                print "pause %s" % self.paused
                print "============"

            #self.paused = not self.paused

    def playRecording(self):
        if self.Player.isPlaying():
            self.Player.pause()
            self.paused = not self.paused
        else:
            dir = self.Settings("video_source")
            #print ("DIR : %s" % dir)
            #print ("Relative %s" % self.Recording.RelativeFileName)
            #print ("type %s" % type(self.Recording.RelativeFileName))
            #print ("Relative %s" % self.Recording.RelativeFileName)
            self.Recording.RelativeFileName = self.Recording.RelativeFileName[1:]
            url = dir + "/" + self.Recording.RelativeFileName.replace("/","_") + ".mpg"
            #print ("URL FILE : %s " % url)
            self.Player.play(item=url, windowed=True)

    def addMark(self):
        position = self.Player.getTime()
        mark = VDRMark("00:00:00.00")

        mark.Hour = int(position / 3600)
        position -= mark.Hour * 3600

        mark.Minute = int(position / 60)
        position -= mark.Minute * 60

        mark.Second = int(position)
        position -= mark.Second

        mark.Frame = int(position * self.getFramesPerSecond())
        self.Marks.append(mark)

        self.Marks = sorted(self.Marks, cmp=VDRMark.compare)
        self.lMarks.reset()
        for m in self.Marks:
            self.lMarks.addItem(m.toString())

        self.updateCutLine()

    def removeSelectedMark(self):
        position = self.lMarks.getSelectedPosition()
        if position != -1:
            self.Marks.pop(position)
        self.lMarks.reset()
        for mark in self.Marks:
            self.lMarks.addItem(mark.toString())

        self.updateCutLine()

    def jumpToPosition(self):
        position = self.lMarks.getSelectedPosition()
        if position != -1:
            mark = self.Marks[position]
            vpos = float(mark.Hour * 3600 + mark.Minute * 60)
            vpos += mark.Second + mark.Frame / self.getFramesPerSecond()
            self.Player.seekTime(vpos)

    def getFramesPerSecond(self):
        return 50

    def getTotalTime(self):
        return self.Player.getTotalTime()

    def __getRecording(self):
        return self.__recording

    def __setRecording(self, recording):
        #print ("REC : %s" % recording)
        self.__recording = recording

    Recording = property(__getRecording, __setRecording)

    def saveMarks(self):
        if len(self.Marks) == 0:
            self.RestfulAPI.deleteMarks(self.Recording.RecordingId)
        else:
            self.RestfulAPI.saveMarks(self.Recording.RecordingId, self.Marks)

    def addCutPart(self, length, cutted):
        __path__ = xbmc.translatePath(sys.modules['__main__'].Addon.getAddonInfo('path')+'/resources/skins/default/media/')

        bg = __path__ +'grey.png'
        if cutted:
            bg = __path__ + 'green.png'

        px = (1280 / self.getTotalTime()) * length

        img = xbmcgui.ControlImage(self.__cutline, 647, int(px), 5, bg)
        self.addControl(img)
        self.CutLineControls.append(img)

        self.__cutline += int(px)

    def updateCutLine(self):
        for control in self.CutLineControls:
            self.removeControl(control)
            del control

        self.CutLineControls = []

        self.Marks = sorted(self.Marks, cmp=VDRMark.compare)
        self.__cutline = 0
        if len(self.Marks) == 0:
            self.addCutPart(self.getTotalTime(), True)
        else:
            cutted = False
            self.addCutPart(self.Marks[0].toSeconds(), cutted)
            for i in range(0, len(self.Marks)):
                cutted = not cutted
                if i == (len(self.Marks) -1):
                    self.addCutPart((self.getTotalTime() - self.Marks[i].toSeconds()), cutted)
                else:
                    self.addCutPart(self.Marks[i+1].toSeconds()-self.Marks[i].toSeconds(), cutted)
