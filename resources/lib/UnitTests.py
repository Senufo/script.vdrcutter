'''
Created on Sep 3, 2011

@author: Michael Eiler
'''
from VDRPersistence import RestfulAPI, VDRRecording, VDRMark
import svdrpsend
import sys

p = RestfulAPI("127.0.0.1", "8002")

marks = []

marks.append(VDRMark("0:15:17.15"))
marks.append(VDRMark("0:34:08.24"))

p.saveMarks(1, marks)


for rec in p.getRecordings():
    print type(rec)
    if type(rec) is VDRRecording:
        for mark in p.getMarks(rec.RecordingId):
            if type(mark) is VDRMark:
                print mark.toString()

#p.deleteMarks(0)

p.cutRecording(1)
print p.isCutterActive()
sys.stdout.softspace=0
while 1:
    if p.isCutterActive():
        print '.'
    else:
        print p.isCutterActive()
        break

#vdr_command = 'HELP'
#vdr_command = 'EDIT 2'
#svdrp = svdrpsend.SVDRP(hostname=options.hostname, port=options.port, debug_dump=options.debug_dump)
#svdrp = svdrpsend.SVDRP()
#svdrp.start_conversation()
#svdrp.send_command(vdr_command)
#svdrp.finish_conversation()
#cmd_result = svdrp.get_full_response()
#for resp_line in cmd_result:
#        print '%s%s%s' % (resp_line.code, resp_line.delim, resp_line.text)
