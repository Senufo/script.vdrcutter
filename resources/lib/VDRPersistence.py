'''
Created on Sep 3, 2011

@author: Michael Eiler, Senufo
'''
import urllib2
import re
from simplejson import JSONDecoder

class VDRRecording(object):
    def __init__(self, recid, name, fileName, relativeFileName, duration):
        self.__recid = int(recid)
        self.__name = name
        self.__fileName = fileName
        self.__relativeFileName = relativeFileName
        self.__duration = int(duration)

    def __setRecordingId(self, recordingId):
        if type(recordingId) is int:
            self.__recid = recordingId

    def __getRecordingId(self):
        return self.__recid

    def __setName(self, name):
        if type(name) is str:
            self.__name = name

    def __getName(self):
        return self.__name

    def __setFileName(self, fileName):
        if type(fileName) is str:
            self.__fileName = fileName

    def __getFileName(self):
        return self.__fileName

    def __setRelativeFileName(self, relativeFileName):
        if type(relativeFileName) is str:
            self.__relativeFileName = relativeFileName

    def __getRelativeFileName(self):
        return self.__relativeFileName

    def __setDuration(self, duration):
        if type(duration) is int:
            self.__duration = duration

    def __getDuration(self):
        return self.__duration

    def compare(rec1, rec2):
        if rec1.Name.lower() == rec2.Name.lower():
            return 0
        elif rec1.Name.lower() > rec2.Name.lower():
            return 1
        else:
            return -1

    RecordingId = property(__getRecordingId, __setRecordingId)
    Name = property(__getName, __setName)
    FileName = property(__getFileName, __setFileName)
    RelativeFileName = property(__getRelativeFileName, __setRelativeFileName)
    Duration = property(__getDuration, __setDuration)

class VDRMark(object):
    def __init__(self, data):
        markstr = str(data)
        r = re.compile("[0-9]{1,2}:[0-9]{2,2}:[0-9]{2,2}[.]{0,1}[0-9]{0,2}")
        if r.match(markstr):
            parts = markstr.split(":")
            self.__hour = int(parts[0])
            self.__minute = int(parts[1])
            parts = parts[2].split(".")
            self.__second = int(parts[0])
            if len(parts) > 1:
                self.__frame = int(parts[1])

    def __getHour(self):
        return self.__hour

    def __setHour(self, hour):
        if type(hour) is int:
            self.__hour = hour

    def __getMinute(self):
        return self.__minute

    def __setMinute(self, minute):
        if type(minute) is int:
            self.__minute = minute

    def __getSecond(self):
        return self.__second

    def __setSecond(self, second):
        if type(second) is int:
            self.__second = second

    def __getFrame(self):
        return self.__frame

    def __setFrame(self, frame):
        if type(frame) is int:
            self.__frame = frame

    def toString(self):
        minute = str(self.Minute)
        if len(minute) == 1:
            minute = "0" + minute
        second = str(self.Second)
        if len(second) == 1:
            second = "0" + second
        frame = str(self.Frame)
        if len(frame) == 1:
            frame = "0" + frame
        return str(self.Hour) + ":" + minute + ":" + second + "." + frame

    def toSeconds(self):
        return self.Hour * 3600 + self.Minute * 60 + self.Second

    def compare(mark2, mark):
        first = mark2.Hour * 3600 + mark2.Minute * 60 + mark2.Second
        second = mark.Hour * 3600 + mark.Minute * 60 + mark.Second

        if first != second:
            return first - second

        return mark2.Frame - mark.Frame

    Hour = property(__getHour, __setHour)
    Minute = property(__getMinute, __setMinute)
    Second = property(__getSecond, __setSecond)
    Frame = property(__getFrame, __setFrame)

class HttpClient(object):
    def __init__(self):
        redirect_handler= urllib2.HTTPRedirectHandler()
        self._opener = urllib2.build_opener(redirect_handler)

    def GET(self, url):
        try:
            return self._opener.open(url).read()
        except:
            return ""

    def DELETE (self, url):
        try:
            opener = urllib2.build_opener(urllib2.HTTPHandler)
            request = urllib2.Request(url)
            request.add_header('Content-Type', 'text/plain')
            request.get_method = lambda: 'DELETE'
            return opener.open(request).read()
        except:
            return ""

    def POST(self, url, data):
        try:
            req = urllib2.Request(url, data)
            return urllib2.urlopen(req).read()
        except:
            return ""

class RestfulAPI(object):
    def __init__(self, ip, port):
        self.__server = HttpClient()
        self.__url = "http://%s:%s"%(ip, port)
        self.JSON = JSONDecoder()
        #print ("URL : %s" % self.__url)
    def __createUrl(self, service):
        return self.__url + "/" + service

    def isCutterActive(self):
        data = self.__server.GET(self.__createUrl("recordings/cut.json"))
        json = self.JSON.decode(data)
        return json["active"]

    def getRecordings(self):
        data = self.__server.GET(self.__createUrl("recordings.json?marks=true"))
        #print ("DATA >%s<" % data)
        json = self.JSON.decode(data)
        recs = []
        for recdata in json["recordings"]:
            recs.append(VDRRecording(recdata["number"], recdata["name"], recdata["file_name"], recdata["relative_file_name"], recdata["duration"]))
        return recs

    def cutRecording(self, recordingnumber):
        service = self.__createUrl("recordings/cut/" + str(recordingnumber))
        return self.__server.POST(service, ' ')
        #Il y a un decalage de 1 entre le recordingnumber de
        #restfulapi et svdrpsend
        recordnb = recordingnumber + 1
        vdr_command = 'EDIT %d' % recordnb
        svdrp = svdrpsend.SVDRP()
        svdrp.start_conversation()
        svdrp.send_command(vdr_command)
        svdrp.finish_conversation()
        cmd_result = svdrp.get_full_response()
        for resp_line in cmd_result:
            print '%s%s%s' % (resp_line.code, resp_line.delim, resp_line.text)

    def getMarks(self, recordingnumber):
        data = self.__server.GET(self.__createUrl("recordings/"+str(recordingnumber)+".json?marks=true"))
        json = self.JSON.decode(data)
        marks = []
        for mark in json["recordings"][0]["marks"]:
            marks.append(VDRMark(mark))
        return marks

    def saveMarks(self, recordingnumber, marks):
        #url = self.__createUrl("recordings/marks/" + str(recordingnumber))
        #json = self.__serializeMarks(marks)
        #print "marks : %s" % marks.
        #print "url : %s" % url
        #print "json : %s" % json
        #Liste l'ensemble des enregistrements
        enrs = self.getRecordings()
        #for enr in enrs:
        #    print "ENR : %s" % enr.FileName
        #Affiche l'enregistrement du recordingnumber
        #Attention il y a un decalage de 1 entre restfulapi et svdrpsend
        print "Nb : %d, Name : %s" % (recordingnumber,enrs[recordingnumber].FileName)
        marks_file = open(enrs[recordingnumber].FileName + '/marks','w')
        #marks_file = enrs[recordingnumber].FileName + '/marks'
        print "markfile :%s" % marks_file
        for mark in marks:
            if type(mark) is VDRMark:
                #print "MARK : %s" % mark.toString()
                signet = mark.toString() + "\n"
                print "signet :%s" % signet
                marks_file.write(signet)
        marks_file.close()
        #self.__server.POST(url, json)

    def deleteMarks(self, recordingnumber):
        self.__server.DELETE(self.__createUrl("recordings/marks/" + str(recordingnumber)))

    def refreshRecording(self, recording):
        if type(recording) == VDRRecording:
            recs = self.getRecordings()
            for rec in recs:
                if rec.FileName == recording.FileName:
                    recording.RecordingId = rec.RecordingId

    def __serializeMarks(self, marks):
        json = "{'marks':["
        counter = 0
        for mark in marks:
            if type(mark) is VDRMark:
                json += "'" + mark.toString() +"'"
                if len(marks)-1 != counter:
                    json += ", "
            counter += 1
        json += "]}"
        print json
        return json
